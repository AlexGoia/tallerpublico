import { Pipe, PipeTransform } from '@angular/core';
import { Item } from './models/item';


@Pipe({
  name: 'showAvailableItem'
})
export class ShowAvailableItemPipe implements PipeTransform {

  transform(items: Item[], activated: boolean, searchText: string): Item[] {
    if (activated) {
      if (searchText) {
        searchText = searchText.toLowerCase();

        return items
        .filter(item => item.available === true)
        .filter(item => item.name.toLowerCase().includes(searchText));
      } else {
        return items.filter(item => item.available === true);
      }
    } else if (searchText) {
      searchText = searchText.toLowerCase();
      return items.filter(item => item.name.toLowerCase().includes(searchText));
    }
    return items;
  }

}
