import { Pipe, PipeTransform } from '@angular/core';
import { en } from './data/en';
import { es } from './data/es';

@Pipe({
  name: 'translations'
})
export class TranslationsPipe implements PipeTransform {
  miLanguage = {
    'es': es,
    'en': en
  };
  transform(key: any, language: any): any {
    return this.miLanguage[language][key] ? this.miLanguage[language][key] : key;
  }

}
