import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Item } from '../models/item';
import { TranslateService } from '../services/translate.service';
import { ManageItemsService } from '../services/manage-items.service';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit, OnDestroy {
  items: Item[];
  idioma: string;
  @Output() itemSelected: EventEmitter<Item>;
  @Output() ubicacionChange: EventEmitter<string>;


  showAvailable: boolean;
  searchText: string;
  isLoading: boolean;
  itemsGet: Observable<any>;

  constructor(
    private translateService: TranslateService,
    private manageItems: ManageItemsService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.showAvailable = false;
    this.itemSelected = new EventEmitter<Item>();
    this.ubicacionChange = new EventEmitter();
    this.translateService.getLanguage().subscribe(
      e => {
        // El valor inicial se establece gracias a BeheavourSubject
        this.idioma = e;
      }
    );
    this.itemsGet = null;
  }

  ngOnInit() {
    this.showSpinner();
    this.manageItems.getItems().subscribe(
      (data: Item[]) => {
        this.items = data;
        this.hideSpinner();
      },
      err => {
        this.hideSpinner();
        console.error('Error:' + err);
      }
    );
  }

  // SEND ITEM
  enviaItem(index: number) {
    this.router.navigate(['item', index]);
    // this.itemSelected.emit(this.items[index]);
  }

  // HIDE SPINNER
  hideSpinner() {
    this.spinner.hide();
  }

  // SHOW SPINNER
  showSpinner() {
    this.spinner.show();
  }

  ngOnDestroy() {
    //
  }

}
