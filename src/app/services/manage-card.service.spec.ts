import { TestBed, inject } from '@angular/core/testing';

import { ManageCardService } from './manage-card.service';

describe('ManageCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManageCardService]
    });
  });

  it('should be created', inject([ManageCardService], (service: ManageCardService) => {
    expect(service).toBeTruthy();
  }));
});
