import { TestBed, inject } from '@angular/core/testing';

import { ManageItemsService } from './manage-items.service';

describe('ManageItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManageItemsService]
    });
  });

  it('should be created', inject([ManageItemsService], (service: ManageItemsService) => {
    expect(service).toBeTruthy();
  }));
});
