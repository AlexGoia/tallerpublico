import { Injectable } from '@angular/core';
import { Item } from '../models/item';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { CardElement } from '../models/cardElement';

@Injectable({
  providedIn: 'root'
})
export class ManageCardService {
  items: CardElement[];
  $numItems: BehaviorSubject<number>;

  constructor() {
    this.items = [];
    this.$numItems = new BehaviorSubject<number>(0);
  }

  addItem(itemToAdd: Item) {
    const elemento = this.items.find( e => {
      return e.item.name === itemToAdd.name;
    });

    if ( elemento) {
      const index = this.items.indexOf(elemento);
      this.items[index] = {item: elemento.item, cantidad: (elemento.cantidad + 1) };

    } else {
      this.items = [...this.items, {item: itemToAdd, cantidad: 1}];
    }
    this.$numItems.next(this.items.length);

  }

  getAllItems() {
    return this.items;
  }

  deleteItem(itemToDelete: Item) {
    const elemento = this.items.find( e => {
      return e.item.name === itemToDelete.name;
    });
    if (elemento) {
      if (elemento.cantidad > 0) {
        elemento.cantidad -= 1;
      } else {
        const index = this.items.indexOf(elemento);
        this.items.splice(index, 1);
      }
    }

  }

  addItemToCard(itemCard: CardElement) {
    const elemento = this.items.find( e => {
      return e.item.name === itemCard.item.name;
    });

    if (elemento) {
      elemento.cantidad = itemCard.cantidad;
      console.error('Servicio: Actualizo cantidad:' + JSON.stringify(elemento));
    } else {
      console.error('Servicio: añado item:' + JSON.stringify(itemCard));
      this.items.push(itemCard);
    }
    this.$numItems.next(this.items.length);

  }
}
