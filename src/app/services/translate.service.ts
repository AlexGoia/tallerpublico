import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  lang: string;
  $langObs: BehaviorSubject<string>;
  constructor() {
    this.lang = 'es';
    this.$langObs = new BehaviorSubject<string>('es');
  }
  getLanguage(): Observable<string> {
    return this.$langObs.asObservable();
  }
  setLanguage(lang: string) {
    this.lang = lang;
    this.$langObs.next(this.lang);
  }
}
