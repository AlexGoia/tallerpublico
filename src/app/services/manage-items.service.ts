import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class ManageItemsService {

  constructor(private httpClient: HttpClient) {

  }
  getItems() {
    return this.httpClient.get('http://localhost:3000/items');
  }
  postItem() {}
  deleteItem() {}

  getItem(id: number): Observable<Item> {
    return this.httpClient.get<Item>('http://localhost:3000/items/' + id);
  }

}
