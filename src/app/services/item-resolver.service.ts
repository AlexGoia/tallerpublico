import { Injectable } from '@angular/core';
import { Resolve, Router, Routes, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Item } from '../models/item';
import { ManageItemsService } from './manage-items.service';


@Injectable({
  providedIn: 'root'
})
export class ItemResolverService implements Resolve<Observable<Item>> {

  constructor(private manageItems: ManageItemsService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Item> {
    const miId = route.params.id;
    return this.manageItems.getItem(miId);
  }


}
