import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TextnavbarComponent } from './textnavbar/textnavbar.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemComponent } from './item/item.component';
import { DetailsComponent } from './details/details.component';
import { ShowAvailableItemPipe } from './show-available-item.pipe';
import { TranslationsPipe } from './translations.pipe';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CarritoComponent } from './carrito/carrito.component';
import { CuantityCardElementComponent } from './cuantity-card-element/cuantity-card-element.component';
import { ConvertMoneyPipe } from './convert-money.pipe';
import { APP_ROUTING } from './app.routes';
import { NotFoundComponent } from './not-found/not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TextnavbarComponent,
    ItemListComponent,
    ItemComponent,
    DetailsComponent,
    ShowAvailableItemPipe,
    TranslationsPipe,
    CarritoComponent,
    CuantityCardElementComponent,
    ConvertMoneyPipe,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    APP_ROUTING
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
