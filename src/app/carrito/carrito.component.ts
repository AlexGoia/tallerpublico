import { Component, OnInit } from '@angular/core';
import { ManageCardService } from '../services/manage-card.service';
import { Item } from '../models/item';
import { CardElement } from '../models/cardElement';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  items: CardElement[];

  constructor(private manageCard: ManageCardService) {
    this.items = [];
    this.items = manageCard.getAllItems();
  }

  ngOnInit() {
  }

  deleteItem(item: Item) {
    this.manageCard.deleteItem(item);
  }

  addItem(item: Item) {
    this.manageCard.addItem(item);
  }

  getTotal() {
    const sumatorio = this.items.map(e => {
      return e.item.price * e.cantidad;
    });
    return sumatorio.reduce((pV, cV) => {
      return pV + cV;
    });
  }


}
