import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Idioma } from '../models/idioma';
import { TranslateService } from '../services/translate.service';
import { ManageCardService } from '../services/manage-card.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() enviarIdioma: EventEmitter<string>;
  @Output() ubicacionChange: EventEmitter<string>;

  languages: Idioma[];
  selectedLanguage: string;
  itemsNumber: number;

  constructor(private translateService: TranslateService, private manageCard: ManageCardService) {
    this.languages = [
      { key: 'es', value: 'Español'},
      { key: 'en', value: 'English'}
    ];
    this.enviarIdioma = new EventEmitter();
    this.ubicacionChange = new EventEmitter();
    this.itemsNumber = 0;
    manageCard.$numItems.subscribe(data => {
      this.itemsNumber = data;
    });
  }

  ngOnInit() {
  }

  enviarHijo() {
    // Envío el idioma seleccionado al padre (event sera un string)
    this.enviarIdioma.emit(this.selectedLanguage);
    this.translateService.setLanguage(this.selectedLanguage);

  }

  enviaUbicacion(ubicacion: string) {
    this.ubicacionChange.emit(ubicacion);
  }

}
