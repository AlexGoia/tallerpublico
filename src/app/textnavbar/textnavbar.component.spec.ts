import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextnavbarComponent } from './textnavbar.component';

describe('TextnavbarComponent', () => {
  let component: TextnavbarComponent;
  let fixture: ComponentFixture<TextnavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextnavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextnavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
