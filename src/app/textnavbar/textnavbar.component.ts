import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '../services/translate.service';

@Component({
  selector: 'app-textnavbar',
  templateUrl: './textnavbar.component.html',
  styleUrls: ['./textnavbar.component.css']
})
export class TextnavbarComponent implements OnInit {
  idioma: string;
  contenido: Map<string, string> = new Map();


  constructor(private translateService: TranslateService) {
    this.contenido.set('es', 'Hola mundo');
    this.contenido.set('en', 'Hello world');
    this.translateService.$langObs.subscribe(
      e => {
      this.idioma = e;
    });
  }

  ngOnInit() {

  }

}
