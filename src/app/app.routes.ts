import { RouterModule, Routes } from '@angular/router';
import { TextnavbarComponent } from './textnavbar/textnavbar.component';
import { ItemListComponent } from './item-list/item-list.component';
import { DetailsComponent } from './details/details.component';
import { CarritoComponent } from './carrito/carrito.component';
import { ItemResolverService } from './services/item-resolver.service';

const APP_ROUTES: Routes = [
    { path: '', component: TextnavbarComponent },
    { path: 'listado', component: ItemListComponent },
    {
        path: 'item/:id',
        component: DetailsComponent,
        resolve: {
            item: ItemResolverService
        }
    },
    { path: 'carrito', component: CarritoComponent },
    { path: '**', component: TextnavbarComponent }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
