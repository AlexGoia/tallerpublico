import { Component, Input } from '@angular/core';
import { Item } from './models/item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ubicacion = 'welcome';
  itemToSend: Item;

  title = 'app';
  idioma = 'es';
  constructor() {

  }

  recibirHijo(evento) {
    this.idioma = evento;
  }

  escuchaUbicacion(ubicacion) {
    this.ubicacion = ubicacion;
  }

  catchItem(item: Item) {
    this.ubicacion = 'detalles';
    this.itemToSend = item;
  }
}
