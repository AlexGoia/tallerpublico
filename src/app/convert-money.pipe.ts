import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertMoney'
})
export class ConvertMoneyPipe implements PipeTransform {

  transform(value: number, lang: string): any {
    let resultado = 0;
    if (lang === 'es') {
      // De € a libras
      resultado = value * 0.9;
    } else if (lang === 'en') {
      // De libras a euros
      resultado = value * 1.1;
    }
    return resultado;
  }

}
