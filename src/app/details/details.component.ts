import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../models/item';
import { ManageCardService } from '../services/manage-card.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  itemSelected: Item;
  @Input() language: string;
  cantidadItems: number;

  constructor(private manageCard: ManageCardService, private route: ActivatedRoute) {
    this.cantidadItems = 0;
    // Esta cantidad no siempre es 0
    this.route.params.subscribe(data => {
      console.log('Data:' + data['id']);
    });

    this.itemSelected = this.route.snapshot.data.item;
    console.log('Snapshot data:' + JSON.stringify(this.route.snapshot.data.item));
  }

  ngOnInit() {

  }

  getCurrencyCode(): string {
    switch (this.language) {
      case 'es':
        return 'EUR';
      case 'en':
        return 'GBP';
    }
  }

  addItem() {
    const nuevoItem = {
      item: this.itemSelected,
      cantidad: this.cantidadItems
    };

    console.warn('Voy a añadir :' + JSON.stringify(nuevoItem));
    this.manageCard.addItemToCard(nuevoItem);
  }

}
