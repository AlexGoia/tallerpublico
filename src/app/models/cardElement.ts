import { Item } from './item';

export interface CardElement {
    item: Item;
    cantidad: number;
}
