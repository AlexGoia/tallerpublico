export interface Item {
    name: string;
    price: number;
    available: boolean;
    description: string;
}
