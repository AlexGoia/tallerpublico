import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../models/item';
import { ManageCardService } from '../services/manage-card.service';

@Component({
  selector: 'app-cuantity-card-element',
  templateUrl: './cuantity-card-element.component.html',
  styleUrls: ['./cuantity-card-element.component.css']
})
export class CuantityCardElementComponent implements OnInit {

  @Input() item: Item;
  @Output() cantidadChange: EventEmitter<number>;
  cantidad: number;

  constructor(private cardService: ManageCardService) {
    this.cantidad = 0;
    this.cantidadChange = new EventEmitter<number>();

  }

  ngOnInit() {
    const elemento = this.cardService.getAllItems().find( e => {
      return e.item.name === this.item.name;
    });
    if (elemento) {
      console.warn('El elemento ya existe');
      this.cantidad = elemento.cantidad;
      this.cantidadChange.emit(this.cantidad);
    }
  }

  deleteItem() {
    if (this.cantidad > 1) {
      this.cantidad --;
    } else {
      this.cantidad = 0;
    }
    this.cantidadChange.emit(this.cantidad);
  }

  addItem() {
    this.cantidad++;
    this.cantidadChange.emit(this.cantidad);
  }
}
