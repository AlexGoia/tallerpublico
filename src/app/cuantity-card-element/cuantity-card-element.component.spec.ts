import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuantityCardElementComponent } from './cuantity-card-element.component';

describe('CuantityCardElementComponent', () => {
  let component: CuantityCardElementComponent;
  let fixture: ComponentFixture<CuantityCardElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuantityCardElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuantityCardElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
